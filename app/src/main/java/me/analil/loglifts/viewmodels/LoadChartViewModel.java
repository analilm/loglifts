package me.analil.loglifts.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import me.analil.data.models.IntensityModel;
import me.analil.data.repository.ILoadChartRepo;
import me.analil.data.repository.LoadChartRepo;
import java.util.List;

public class LoadChartViewModel extends AndroidViewModel {
    private static final String TAG = LoadChartViewModel.class.getSimpleName();
    private ILoadChartRepo mLoadChartRepo;

    public LoadChartViewModel(@NonNull Application app) {
        super(app);
        this.mLoadChartRepo = LoadChartRepo.create(app);
    }

    @VisibleForTesting
    public LoadChartViewModel(@NonNull Application app, LoadChartRepo repo) {
        super(app);
        this.mLoadChartRepo = repo;
    }

    public LiveData<List<IntensityModel>> getLoadChart() { return mLoadChartRepo.getLoadChart(); }

    public LiveData<String> getErrorUpdates() {
        return mLoadChartRepo.getErrorStream();
    }

    public void fetchData() {
        mLoadChartRepo.fetchData();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "onCleared: called");
    }
}
