package me.analil.loglifts.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import me.analil.loglifts.R;
import me.analil.loglifts.fragments.CalcFragment;
import me.analil.loglifts.fragments.LogFragment;
import me.analil.loglifts.fragments.StrengthFragment;
import me.analil.loglifts.models.FragmentTag;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int STRENGTH_FRAGMENT = 0;
    private static final int LOG_FRAGMENT = 1;
    private static final int CALC_FRAGMENT = 2;
    private int mExitCount = 0;
    private ArrayList<String> mFragmentTags = new ArrayList<>();
    private ArrayList<FragmentTag> mFragments = new ArrayList<>();
    private FragmentTransaction mTransaction;
    private BottomNavigationView mBottomNavigationView;
    private CalcFragment mCalcFragment;
    private LogFragment mLogFragment;
    private StrengthFragment mStrengthFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_strength:
                    mFragmentTags.clear();
                    mFragmentTags = new ArrayList<>();
                    initMainFragment();
                    item.setChecked(true);
                    return true;
                case R.id.navigation_log:
                    if(mLogFragment == null) {
                        mLogFragment = new LogFragment();
                        mTransaction = getSupportFragmentManager().beginTransaction();
                        mTransaction.add(R.id.content_main, mLogFragment, getString(R.string.tag_fragment_log));
                        mTransaction.commit();
                        mFragmentTags.add(getString(R.string.tag_fragment_log));
                        mFragments.add(new FragmentTag(mLogFragment, getString(R.string.tag_fragment_log)));
                    } else {
                        mFragmentTags.remove(getString(R.string.tag_fragment_log));
                        mFragmentTags.add(getString(R.string.tag_fragment_log));
                    }
                    item.setChecked(true);
                    setFragmentVisibilities(getString(R.string.tag_fragment_log));
                    return true;
                case R.id.navigation_calc:
                    if(mCalcFragment == null) {
                        mCalcFragment = new CalcFragment();
                        mTransaction = getSupportFragmentManager().beginTransaction();
                        mTransaction.add(R.id.content_main, mCalcFragment, getString(R.string.tag_fragment_calc));
                        mTransaction.commit();
                        mFragmentTags.add(getString(R.string.tag_fragment_calc));
                        mFragments.add(new FragmentTag(mCalcFragment, getString(R.string.tag_fragment_calc)));
                    } else {
                        mFragmentTags.remove(getString(R.string.tag_fragment_calc));
                        mFragmentTags.add(getString(R.string.tag_fragment_calc));
                    }
                    item.setChecked(true);
                    setFragmentVisibilities(getString(R.string.tag_fragment_calc));
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBottomNavigationView = findViewById(R.id.navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initMainFragment();
    }

    @Override
    public void onBackPressed() {
        int backStackCount = mFragmentTags.size();
        if(backStackCount > 1) {
            String top = mFragmentTags.get(backStackCount - 1);
            String belowTop = mFragmentTags.get(backStackCount - 2);
            setFragmentVisibilities(belowTop);
            mFragmentTags.remove(top);
            mExitCount = 0;
        } else if (backStackCount == 1) {
            mExitCount++;
        }
        if(mExitCount >= 2) {
            super.onBackPressed();
        }
    }

    private void initMainFragment() {
        if(mStrengthFragment == null) {
            mStrengthFragment = new StrengthFragment();
            mTransaction = getSupportFragmentManager().beginTransaction();
            mTransaction.add(R.id.content_main, mStrengthFragment, getString(R.string.tag_fragment_strength));
            mTransaction.commit();
            mFragmentTags.add(getString(R.string.tag_fragment_strength));
            mFragments.add(new FragmentTag(mStrengthFragment, getString(R.string.tag_fragment_strength)));
        } else {
            mFragmentTags.remove(getString(R.string.tag_fragment_strength));
            mFragmentTags.add(getString(R.string.tag_fragment_strength));
        }
        setFragmentVisibilities(getString(R.string.tag_fragment_strength));
    }

    private void setFragmentVisibilities(String tag) {
        for(int i = 0; i < mFragments.size(); i++) {
            if(tag.equals(mFragments.get(i).getTag())) {
                mTransaction = getSupportFragmentManager().beginTransaction();
                mTransaction.show(mFragments.get(i).getFragment());
                mTransaction.commit();
            } else {
                mTransaction = getSupportFragmentManager().beginTransaction();
                mTransaction.hide(mFragments.get(i).getFragment());
                mTransaction.commit();
            }
        }
        setNavIcon(tag);
    }

    private void setNavIcon(String tag) {
        Menu menu = mBottomNavigationView.getMenu();
        MenuItem menuItem;
        if(tag.equals(getString(R.string.tag_fragment_strength))) {
            menuItem = menu.getItem(STRENGTH_FRAGMENT);
            menuItem.setChecked(true);
        } else if(tag.equals(getString(R.string.tag_fragment_log))) {
            menuItem = menu.getItem(LOG_FRAGMENT);
            menuItem.setChecked(true);
        } else if(tag.equals(getString(R.string.tag_fragment_calc))) {
            menuItem = menu.getItem(CALC_FRAGMENT);
            menuItem.setChecked(true);
        }
    }
}