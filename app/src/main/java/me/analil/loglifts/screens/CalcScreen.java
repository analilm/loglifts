package me.analil.loglifts.screens;

import me.analil.data.models.IntensityModel;
import java.util.List;

public interface CalcScreen {
    void updateData(List<IntensityModel> data);
    void setError(String errorMsg);
}