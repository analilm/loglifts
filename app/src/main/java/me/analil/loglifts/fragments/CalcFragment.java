package me.analil.loglifts.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import me.analil.data.models.IntensityModel;
import me.analil.loglifts.R;
import me.analil.loglifts.screens.CalcScreen;
import me.analil.loglifts.viewmodels.LoadChartViewModel;

import java.util.List;

public class CalcFragment extends android.support.v4.app.Fragment implements CalcScreen {
    private static final String TAG = CalcFragment.class.getSimpleName();
    private final static int DATA_FETCHING_INTERVAL = 5*1000; //5 seconds
    private long mLastFetchedDataTimeStamp;
    private LoadChartViewModel mViewModel;
    private CalcViewAdapter mAdapter;

    public CalcFragment() {}

    private final Observer<List<IntensityModel>> dataObserver = new Observer<List<IntensityModel>>() {
        @Override
        public void onChanged(@Nullable List<IntensityModel> intensityModels) {
                CalcFragment.this.updateData(intensityModels);
            }
        };

    private final Observer<String> errorObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String errorMsg) {
                CalcFragment.this.setError(errorMsg);
            }
        };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calc_recv, container, false);

        if(view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            mAdapter = new CalcViewAdapter();
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(LoadChartViewModel.class);
        mViewModel.getLoadChart().observe(this, dataObserver);
        mViewModel.getErrorUpdates().observe(this, errorObserver);
        mViewModel.fetchData();
    }

    @Override
    public void updateData(List<IntensityModel> data) {
        mLastFetchedDataTimeStamp = System.currentTimeMillis();
        mAdapter.setItems(data);
    }

    private void showErrorToastMsg(String errorMsg) { Toast.makeText(getActivity(), "Error: " + errorMsg, Toast.LENGTH_LONG).show(); }

    @Override
    public void setError(String errorMsg) {
        showErrorToastMsg(errorMsg);
    }

    @Override
    public void onAttach(Context context) { super.onAttach(context); }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
