package me.analil.loglifts.fragments;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import me.analil.data.models.IntensityModel;
import me.analil.loglifts.R;
import java.util.ArrayList;
import java.util.List;

public class CalcViewAdapter extends RecyclerView.Adapter<CalcViewAdapter.CalcViewHolder> {

    private List<IntensityModel> mItems = new ArrayList<>();;
    private final Handler mHandler = new Handler();

    class CalcViewHolder extends RecyclerView.ViewHolder {
        private final TextView primary_id;
        private final TextView percentage;
        private final TextView reps;
        private final TextView weight;

        private CalcViewHolder(View itemView) {
            super(itemView);
            primary_id = itemView.findViewById(R.id.primary_id);
            percentage = itemView.findViewById(R.id.percentage);
            reps = itemView.findViewById(R.id.reps);
            weight = itemView.findViewById(R.id.weight);
        }
    }

    @Override
    public CalcViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CalcViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_calc_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CalcViewHolder holder, int position) {
            final IntensityModel model = mItems.get(position);
            holder.primary_id.setText(String.format(model.id));
            holder.percentage.setText(String.format(model.percentage));
            holder.reps.setText(String.format(model.reps));
            holder.weight.setText(String.format(model.weight));
    }

    @Override
    public int getItemCount() {
        if(mItems != null) {
            return mItems.size();
        } else {
            return 0;
        }
    }

    public void setItems(List<IntensityModel> items) {
        this.mItems.clear();
        this.mItems.addAll(items);
        notifyDataSetChanged();
    }
}

