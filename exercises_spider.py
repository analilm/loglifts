import scrapy
import re
from scrapy.linkextractors import LinkExtractor

filename = 'exercises.txt'
count = 0
class ExercisesSpider(scrapy.Spider):
    name = 'exercises_spider'

    def start_requests(self):
        start_urls = ['https://exrx.net/Lists/OlympicWeightlifting']
        
        for url in start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        lifts_data = response.xpath('//@href').re('OlympicLifts/(.*)')
        weightlifting_data = response.xpath('//@href').re('WeightExercises/(?!OlympicLifts).*/(.*)')
        exercise_set = set()

        for w in weightlifting_data:
            lifts_data.append(w)
                
        for exercise in lifts_data:
            exercise_set.add(exercise)

        sorted_list_of_exercises = []
        for e in exercise_set:
            if(re.match(r'(\w)([A-Z])([A-Z])', e)):
                ex = re.sub(r'(\w)(?=[A-Z])([A-Z])', r'\1 \2', e[2:])
                exer = e[:2] + ' ' + ex
                sorted_list_of_exercises.append(exer)
            else:
                ex = re.sub(r'(\w)([A-Z])', r'\1 \2', e)
                sorted_list_of_exercises.append(ex)
                
        sorted_list_of_exercises.sort()

        with open(filename, 'w') as file:
            for e in sorted_list_of_exercises:
                file.write(e + '\n')
                
