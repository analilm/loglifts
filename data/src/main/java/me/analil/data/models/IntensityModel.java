package me.analil.data.models;

import android.support.annotation.NonNull;

public class IntensityModel {
    public final String id;
    public final String reps;   // 1-12
    public final String percentage; // 100, 95, 93, 90, 87, 85, 83, 80, 77, 75, 70
    public final String weight;

    public IntensityModel(@NonNull String id, @NonNull String reps, @NonNull String percentage, @NonNull String weight) {
        this.id = id;
        this.reps = reps;
        this.percentage = percentage;
        this.weight = weight;
    }
}
