package me.analil.data.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import me.analil.data.entities.IntensityEntity;
import java.util.List;

@Dao
public interface IntensityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIntensity(List<IntensityEntity> intensities);

    @Query("SELECT * FROM load_chart")
    LiveData<List<IntensityEntity>> getLoadChartLive();

    @Query("SELECT * FROM load_chart")
    List<IntensityEntity> getLoadChart();

    @Query("DELETE FROM load_chart")
    void deleteAll();

    @Insert
    void insert(IntensityEntity entity);
}
