package me.analil.data.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import me.analil.data.R;
import me.analil.data.entities.IntensityEntity;

@Database(entities = {IntensityEntity.class}, version = 1)
public abstract class RoomDb extends RoomDatabase {
    private static RoomDb INSTANCE;
    public abstract IntensityDao intensityDao();

    public static RoomDb getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    RoomDb.class, String.valueOf(R.string.DATABASE_NAME)).build();
        }
        return INSTANCE;
    }
}