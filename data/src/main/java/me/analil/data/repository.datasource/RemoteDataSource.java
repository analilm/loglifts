package me.analil.data.repository.datasource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import me.analil.data.R;
import me.analil.data.entities.IntensityEntity;
import me.analil.data.mappers.Mapper;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;

public class RemoteDataSource implements IDataSource<List<IntensityEntity>> {
    private static final String TAG = RemoteDataSource.class.getSimpleName();
    public final String ENDPOINT_FETCH_LIFTS_LOAD_CHART = "https://my-json-server.typicode.com/azhyur/lifts/loadChart/";
    private final RequestQueue mQueue;
    private final Mapper mObjMapper;
    private final MutableLiveData<String> mError = new MutableLiveData<>();
    private final MutableLiveData<List<IntensityEntity>> mDataApi = new MutableLiveData<>();

    public RemoteDataSource(Context appContext, Mapper objMapper) {
        mQueue = Volley.newRequestQueue(appContext);
        mObjMapper = objMapper;
    }

    @Override
    public LiveData<List<IntensityEntity>> getDataStream() {
        return mDataApi;
    }

    @Override
    public LiveData<String> getErrorStream() {
        return mError;
    }

    public void fetch() {
        final JsonArrayRequest jsonObjReq =
                new JsonArrayRequest(ENDPOINT_FETCH_LIFTS_LOAD_CHART,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                Log.d(TAG, "Thread->" + Thread.currentThread().getName() + "\tGot some network response");
                                final ArrayList<IntensityEntity> data = mObjMapper.mapJSONToEntity(response.toString());
                                mDataApi.setValue(data);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Thread->" + Thread.currentThread().getName() + "\tGot network error");
                                mError.setValue(error.toString());
                            }
                        });
        mQueue.add(jsonObjReq);
    }
}
