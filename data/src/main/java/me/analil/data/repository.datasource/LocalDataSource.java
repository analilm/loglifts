package me.analil.data.repository.datasource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import me.analil.data.db.RoomDb;
import me.analil.data.entities.IntensityEntity;
import java.util.List;

public class LocalDataSource implements IDataSource<List<IntensityEntity>> {
    private final RoomDb mDb;
    private final MutableLiveData<String> mError = new MutableLiveData<>();

    public LocalDataSource(Context mAppContext) {
        mDb = RoomDb.getDatabase(mAppContext);
    }

    public void writeData(List<IntensityEntity> intensities) {
        try {
            mDb.intensityDao().insertIntensity(intensities);
        } catch(Exception e)
        {
            e.printStackTrace();
            mError.postValue(e.getMessage());
        }
    }

    public List<IntensityEntity> getLoadChart() { return mDb.intensityDao().getLoadChart(); }

    @Override
    public LiveData<List<IntensityEntity>> getDataStream() {
        return mDb.intensityDao().getLoadChartLive();
    }

    @Override
    public LiveData<String> getErrorStream() {
        return mError;
    }
}