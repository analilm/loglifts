package me.analil.data.repository.datasource;

import android.arch.lifecycle.LiveData;

public interface IDataSource<T> {
    LiveData<T> getDataStream();
    LiveData<String> getErrorStream();
}
