package me.analil.data.mappers;

import android.support.annotation.NonNull;
import android.util.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.analil.data.entities.IntensityEntity;
import me.analil.data.models.IntensityModel;
import java.util.ArrayList;
import java.util.List;

public class Mapper extends ObjectMapper {
    private static String TAG = Mapper.class.getSimpleName();

    public ArrayList<IntensityEntity> mapJSONToEntity(String jsonStr) {
        ArrayList<IntensityEntity> data = null;
        try {
            data = readValue(jsonStr, new TypeReference<ArrayList<IntensityEntity>>(){});
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return data;
    }

    @NonNull
    public List<IntensityModel> mapEntityToModel(List<IntensityEntity> data) {
        final ArrayList<IntensityModel> listData = new ArrayList<>();
        IntensityEntity entity;
        for (int i = 0; i < data.size(); i++) {
            entity = data.get(i);
            listData.add(new IntensityModel(entity.getId(), entity.getReps(), entity.getPercentage(), entity.getWeight()));
        }
        return listData;
    }

    public String mapEntitiesToString(List<IntensityEntity> data) throws JsonProcessingException {
        return writeValueAsString(data);
    }
}
