package me.analil.data.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(tableName = "load_chart")
public class IntensityEntity {

    public IntensityEntity(String id, String percentage, String reps, String weight) {
        this.id = id;
        this.percentage = percentage;
        this.reps = reps;
        this.weight = weight;
    }

    @Ignore
    public IntensityEntity() {}

    @NonNull
    @PrimaryKey
    @JsonProperty("id")
    private String id;

    @JsonProperty("reps")
    @ColumnInfo(name="reps")
    private String reps;

    @JsonProperty("percentage")
    @ColumnInfo(name="percentage")
    private String percentage;

    @JsonProperty("weight")
    @ColumnInfo(name="weight")
    private String weight;

    @JsonProperty("id")
    public String getId() { return id; }
    @JsonProperty("id")
    public void setId(String id) { this.id = id; }

    @JsonProperty("reps")
    public String getReps() { return reps; }
    @JsonProperty("reps")

    public void setReps(String reps) { this.reps = reps; }
    @JsonProperty("percentage")
    public String getPercentage() { return percentage; }
    @JsonProperty("percentage")
    public void setPercentage(String percentage) { this.percentage = percentage; }

    @JsonProperty("weight")
    public String getWeight() { return weight; }
    @JsonProperty("weight")
    public void setWeight(String weight) { this.weight = weight; }
}
