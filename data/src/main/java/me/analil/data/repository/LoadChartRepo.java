package me.analil.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import me.analil.data.entities.IntensityEntity;
import me.analil.data.mappers.Mapper;
import me.analil.data.models.IntensityModel;
import me.analil.data.repository.datasource.LocalDataSource;
import me.analil.data.repository.datasource.RemoteDataSource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoadChartRepo implements ILoadChartRepo {
    private static final String TAG = LoadChartRepo.class.getSimpleName();
    private ExecutorService mExecutor = Executors.newScheduledThreadPool(2);
    private final RemoteDataSource mRemoteDataSource;
    private final LocalDataSource mLocalDataSource;
    private final Mapper mMapper;
    MediatorLiveData<List<IntensityModel>> mDataMerger = new MediatorLiveData<>();
    MediatorLiveData<String> mErrorMerger = new MediatorLiveData<>();

    private LoadChartRepo(RemoteDataSource remoteDataSource, LocalDataSource localDataSource, Mapper mapper) {
        this.mRemoteDataSource = remoteDataSource;
        this.mLocalDataSource = localDataSource;
        this.mMapper = mapper;
        mDataMerger.addSource(this.mRemoteDataSource.getDataStream(), new Observer<List<IntensityEntity>>() {
                    @Override
                    public void onChanged(@Nullable final List<IntensityEntity> entities) {
                        mExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "mDataMerger\tmRemoteDataSource onChange invoked");
                                mLocalDataSource.writeData(entities);
                                List<IntensityModel> list = mMapper.mapEntityToModel(entities);
                                mDataMerger.postValue(list);
                            }
                        });
                    }
                }
        );

        mDataMerger.addSource(this.mLocalDataSource.getDataStream(), new Observer<List<IntensityEntity>>() {
                    @Override
                    public void onChanged(@Nullable final List<IntensityEntity> entities) {
                        mExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "mDataMerger\tmLocalDataSource onChange invoked");
                                List<IntensityModel> models = mMapper.mapEntityToModel(entities);
                                mDataMerger.postValue(models);
                            }
                        });
                    }
                }
        );

        mErrorMerger.addSource(mRemoteDataSource.getErrorStream(), new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String errorStr) {
                        mErrorMerger.setValue(errorStr);
                        Log.d(TAG, "Network error -> fetching from LocalDataSource");
                        mExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                List<IntensityEntity> entities = (mLocalDataSource.getLoadChart());
                                mDataMerger.postValue(mMapper.mapEntityToModel(entities));
                            }
                        });
                    }
                }
        );
        mErrorMerger.addSource(mLocalDataSource.getErrorStream(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String errorStr) {
                mErrorMerger.setValue(errorStr);
            }
        });
    }

    public static ILoadChartRepo create(Context mAppContext) {
        final Mapper mapper = new Mapper();
        final RemoteDataSource remoteDataSource = new RemoteDataSource(mAppContext, mapper);
        final LocalDataSource localDataSource = new LocalDataSource(mAppContext);
        return new LoadChartRepo(remoteDataSource, localDataSource, mapper);
    }

    @VisibleForTesting
    public static LoadChartRepo createImpl(Context mAppContext) {
        final Mapper mapper = new Mapper();
        final RemoteDataSource remoteDataSource = new RemoteDataSource(mAppContext, mapper);
        final LocalDataSource localDataSource = new LocalDataSource(mAppContext);
        return new LoadChartRepo(remoteDataSource, localDataSource, mapper);
    }

    @Override
    public void fetchData() {
        mRemoteDataSource.fetch();
    }

    @Override
    public LiveData<List<IntensityModel>> getLoadChart() {
        return mDataMerger;
    }

    @Override
    public LiveData<String> getErrorStream() {
        return mErrorMerger;
    }
}

