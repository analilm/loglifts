package me.analil.data.repository;

import android.arch.lifecycle.LiveData;
import me.analil.data.models.IntensityModel;

import java.util.List;

public interface ILoadChartRepo {
    LiveData<List<IntensityModel>> getLoadChart();
    LiveData<String> getErrorStream();
    void fetchData();
}
